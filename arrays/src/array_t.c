#include <stdio.h>
#include <stdlib.h>

#include "arr_t.h"

arr_t*
new_arr_t(int capacity, int element_size)
{
    arr_t* new = malloc(sizeof(arr_t));

    new->size = 0;
    new->element_size = element_size;
    new->capacity = capacity;
    new->data = malloc(capacity * element_size);

    return new;
}

int
is_full_arr_t(arr_t* arr)
{
    return arr->size == arr->capacity;
}

int
append_arr_t(arr_t* arr, void* data)
{
    if (is_full_arr_t(arr))
        return EXIT_FAILURE;

    void* target = (char*)arr->data + (arr->size++ * arr->element_size);

    memcpy(target, data, arr->element_size);

    return EXIT_SUCCESS;
}

// deprecated as this function is less efficient
// than the juggling method, i.e. O(n * length)
// see rotate_arr_t()
int
rotate_left(arr_t* arr)
{
    void* tmp = malloc(arr->element_size);

    memcpy(tmp, arr->data, arr->element_size);

    int i;
    for (i = 0; i < arr->size - 1; i++)
        memcpy((char*)arr->data + (i * arr->element_size), (char*)arr->data + ((i + 1) * arr->element_size), arr->element_size);

    memcpy(arr->data + (i * arr->element_size), tmp, arr->element_size);

    free(tmp);

    return EXIT_SUCCESS;
}

// the gcd is the greatest common
// denominator between two numbers
int
gcd(int a, int b)
{
    while (b != 0)
    {
        int tmp = a;
        a = b;
        b = tmp % b;
    }

    return a;
}

// rotate_arr_t will rotate the data array
// inside the arr struct by a specified
// length
// e.g.
// {1, 2, 3} rotate by 2
// will become {3, 1, 2}

// juggling algorithm to rotate an array
// best case time complexity is O(n)
// worst case is same as rotating by 1
// i.e. O(n * length)
// space complexity O(1)
int
rotate_arr_t(arr_t* arr, int length)
{
    if (length > arr->size)
        return EXIT_FAILURE;

    if (!length)
        return EXIT_FAILURE;

    int gcd_arr = gcd(arr->size, length);

    int i, j, k;
    for (i = 0; i < gcd_arr; i++)
    {

        // hold the initial value temporarily
        void* tmp = malloc(arr->element_size);
        memcpy(tmp, arr->data + (i * arr->element_size), arr->element_size);

        j = i;
        while (1)
        {
            k = j + length;

            if (k >= arr->size)
                k = k - arr->size;

            if (k == i)
                break;

            // rotate the value directly to its intended location
            // as opposed to rotating one at a time
            memcpy((char*)arr->data + (j * arr->element_size), (char*)arr->data + (k * arr->element_size), arr->element_size);

            j = k;
        }

        memcpy(arr->data + (j * arr->element_size), tmp, arr->element_size);
    }

    return EXIT_SUCCESS;
}

int
destroy_arr_t(arr_t* arr)
{
    free(arr->data);
    free(arr);

    return EXIT_SUCCESS;
}
