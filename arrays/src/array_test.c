#include <stdio.h>
#include <stdlib.h>

#include "array_t.c"

// testing macros
#define FAIL() printf("\nFAILURE in %s %s:%d \n", __FILE__, __func__, __LINE__)
#define _assert(test) do { if (!(test)) { FAIL(); return 1; } } while(0)
#define _verify(test) do { int r=test(); tests_run++; if(r) return r; } while(0)

int static tests_run = 0;

int
test_new_arr_t()
{
    int capacity_expected = 10;
    int element_size_expected = sizeof(int);

    arr_t* actual = new_arr_t(capacity_expected, element_size_expected);

    _assert(actual->size == 0);
    _assert(actual->element_size == element_size_expected);
    _assert(actual->capacity == capacity_expected);

    destroy_arr_t(actual);

    return EXIT_SUCCESS;
}

int
test_append_arr_t()
{
    int capacity_expected = 10;
    int element_size_expected = sizeof(int);

    arr_t* actual = new_arr_t(capacity_expected, element_size_expected);

    int data = 1;

    int err = append_arr_t(actual, &data);

    int actual_data;

    memcpy(&actual_data, actual->data, element_size_expected);

    _assert(actual_data == data);
    _assert(actual->size == 1);
    _assert(err == 0);

    int data_again = 2;

    err = append_arr_t(actual, &data_again);

    int actual_data_again;

    memcpy(&actual_data_again, (char*)actual->data + (1 * element_size_expected), element_size_expected);

    _assert(actual_data_again == data_again);
    _assert(actual->size == 2);
    _assert(err == 0);

    destroy_arr_t(actual);

    return EXIT_SUCCESS;
}

int
test_is_full_arr_t()
{
    int capacity_expected = 3;
    int element_size_expected = sizeof(int);

    arr_t* actual = new_arr_t(capacity_expected, element_size_expected);

    int data = 1;

    int err = append_arr_t(actual, &data);

    _assert(err == 0);
    _assert(is_full_arr_t(actual) == 0);

    err = append_arr_t(actual, &data);

    err = append_arr_t(actual, &data);

    _assert(is_full_arr_t(actual) == 1);

    err = append_arr_t(actual, &data);

    _assert(err == 1);

    destroy_arr_t(actual);

    return EXIT_SUCCESS;
}

int
test_rotate_arr_t()
{
    int capacity_expected = 12;
    int element_size_expected = sizeof(int);

    arr_t* actual = new_arr_t(capacity_expected, element_size_expected);

    for (int i = 0; i < capacity_expected; i++)
    {
        int tmp = i;
        int err = append_arr_t(actual, &tmp);
        _assert(err == 0);
    }


    int err = rotate_arr_t(actual, 4);

    _assert(err == 0);

    int expected[] = {4, 5, 6, 7, 8, 9, 10, 11, 0 , 1, 2, 3};

    for (int i = 0; i < 12; i++)
    {
        int tmp;

        memcpy(&tmp, (char*)actual->data + (i * element_size_expected), element_size_expected);

        _assert(tmp == expected[i]);
    }

    destroy_arr_t(actual);

    return EXIT_SUCCESS;
}

int
test_gcd()
{
    _assert(gcd(12, 4) == 4);
    _assert(gcd(10, 6) == 2);
    _assert(gcd(4, 1) == 1);
    _assert(gcd(10, 0) == 10);

    return EXIT_SUCCESS;
}

int
all_arr_tests()
{
    _verify(test_new_arr_t);
    _verify(test_append_arr_t);
    _verify(test_is_full_arr_t);
    _verify(test_rotate_arr_t);
    _verify(test_gcd);

    printf("SUCCESS in %s: ran %d tests\n", __FILE__, tests_run);

    return EXIT_SUCCESS;
};

int
main()
{
    // array of function pointers
    // to hold all our tests
    int total_tests = 1;

    int (*tests[])() = {
        all_arr_tests,
    };

    int i = 0, result = 0;

    while (1)
    {
        result = tests[i++]();

        if (result)
            return EXIT_FAILURE;

        if (i == total_tests)
            break;
    };

    printf("PASSED\n");
    return EXIT_SUCCESS;
}
