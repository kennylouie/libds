#ifndef ARR_H
#define ARR_H

// Describe an array that can be
// instantiated with any generic type
// by user
typedef struct Arr {
    int size;
    int element_size;
    int capacity;
    void* data;
} arr_t;

arr_t* new_arr_t(int capacity, int element_size);

int append_arr_t(arr_t* arr, void* data);

int rotate_arr_t(arr_t* arr, int length);

int destroy_arr_t(arr_t* arr);

#endif
