#include <stdio.h>
#include <stdlib.h>

#include "stack_arr_t.h"

stack_arr_t*
new_stack_arr(int capacity, unsigned element_size)
{
    stack_arr_t* new = malloc(sizeof(stack_arr_t));

    new->top = -1;

    new->capacity = capacity;

    new->element_size = element_size;

    new->data = malloc(element_size * capacity);

    return new;
};

int
stack_arr_is_empty(stack_arr_t* s)
{
    return s->top == -1;
};

int
extend_stack_arr(stack_arr_t* s)
{
    s->data = realloc(s->data, s->capacity * 2);
    s->capacity *= 2;

    return EXIT_SUCCESS;
};

int
stack_arr_is_full(stack_arr_t* s)
{
    return s->top == s->capacity - 1;
};

int
push_stack_arr(stack_arr_t* s, void* element)
{
    if (stack_arr_is_full(s))
    {
        int err = extend_stack_arr(s);
        if (err)
            return EXIT_FAILURE;
    };

    void* target = (char*)s->data + (++s->top * s->element_size);
    memcpy(target, element, s->element_size);

    return EXIT_SUCCESS;
}

int
pop_stack_arr(stack_arr_t* s, void* element)
{
    if (stack_arr_is_empty(s))
        return EXIT_FAILURE;

    void* source = (char*)s->data + (s->top-- * s->element_size);

    memcpy(element, source, s->element_size);

    return EXIT_SUCCESS;
};

int
peek_stack_arr(stack_arr_t* s, void* element)
{
    if (stack_arr_is_empty(s))
        return EXIT_FAILURE;

    void* source = (char*)s->data + (s->top * s->element_size);

    memcpy(element, source, s->element_size);

    return EXIT_SUCCESS;
};

int
destroy_stack_arr(stack_arr_t* s)
{
    free(s->data);

    free(s);

    return EXIT_SUCCESS;
}
