#include <stdio.h>
#include <stdlib.h>

#include "stack_arr_t.c"

// testing macros
#define FAIL() printf("\nFAILURE in %s %s:%d \n", __FILE__, __func__, __LINE__)
#define _assert(test) do { if (!(test)) { FAIL(); return 1; } } while(0)
#define _verify(test) do { int r=test(); tests_run++; if(r) return r; } while(0)

int static tests_run = 0;

int
test_create_stack_arr()
{
    int capacity = 2;
    int element_size = sizeof(int);

    stack_arr_t* s = new_stack_arr(capacity, element_size);

    _assert(s->capacity == capacity);
    _assert(s->element_size == (unsigned)element_size);
    _assert(s->top == -1);
    _assert(sizeof((char*)s->data) == capacity * element_size);

    destroy_stack_arr(s);

    return EXIT_SUCCESS;
};

int
test_stack_arr_capacity()
{
    int capacity = 2;
    int element_size = sizeof(int);

    stack_arr_t* s = new_stack_arr(capacity, element_size);

    _assert(stack_arr_is_full(s) == 0);
    _assert(stack_arr_is_empty(s) == 1);

    destroy_stack_arr(s);

    return EXIT_SUCCESS;
};

int
test_stack_arr_push()
{
    int capacity = 2;
    int element_size = sizeof(int);

    stack_arr_t* s = new_stack_arr(capacity, element_size);

    int d = 10;
    push_stack_arr(s, (void*)&d);

    int actual;
    memcpy(&actual, (char*)s->data + (s->top * s->element_size), s->element_size);

    _assert(d == actual);

    destroy_stack_arr(s);

    return EXIT_SUCCESS;
};

int test_stack_arr_extend()
{
    int capacity = 2;
    int element_size = sizeof(int);

    stack_arr_t* s = new_stack_arr(capacity, element_size);

    int d = 10, e = 10, f = 10;
    push_stack_arr(s, (void*)&d);
    push_stack_arr(s, (void*)&e);
    push_stack_arr(s, (void*)&f);

    int actual;
    memcpy(&actual, (char*)s->data + (s->top * s->element_size), s->element_size);

    _assert(f == actual);
    _assert(s->capacity == capacity * 2);

    destroy_stack_arr(s);

    return EXIT_SUCCESS;
};

int
test_stack_arr_pop()
{
    int capacity = 2;
    int element_size = sizeof(int);

    stack_arr_t* s = new_stack_arr(capacity, element_size);

    int d = 10;
    int actual;
    int err = pop_stack_arr(s, (void*)&actual);

    _assert(err == 1);

    push_stack_arr(s, (void*)&d);

    pop_stack_arr(s, (void*)&actual);

    _assert(d == actual);

    destroy_stack_arr(s);

    return EXIT_SUCCESS;
};

int
test_stack_arr_peek()
{
    int capacity = 2;
    int element_size = sizeof(int);

    stack_arr_t* s = new_stack_arr(capacity, element_size);

    int d = 10;

    int actual;
    int err = peek_stack_arr(s, (void*)&actual);
    _assert(err == 1);

    push_stack_arr(s, (void*)&d);

    peek_stack_arr(s, (void*)&actual);

    _assert(d == actual);

    destroy_stack_arr(s);

    return EXIT_SUCCESS;
};

int
all_stack_arr_tests()
{
    _verify(test_create_stack_arr);
    _verify(test_stack_arr_capacity);
    _verify(test_stack_arr_push);
    _verify(test_stack_arr_pop);
    _verify(test_stack_arr_peek);
    _verify(test_stack_arr_extend);

    printf("SUCCESS in %s: ran %d tests\n", __FILE__, tests_run);

    return EXIT_SUCCESS;
}
