#include <stdlib.h>

#include "stack_arr_test.h"
#include "stack_ll_test.h"

int
main()
{
    // array of function pointers
    // to hold all our tests
    int total_tests = 2;

    int (*tests[])() = {
        all_stack_arr_tests,
        all_stack_ll_tests
    };

    int i = 0, result = 0;

    while (1)
    {
        result = tests[i++]();

        if (result)
            return EXIT_FAILURE;

        if (i == total_tests)
            break;
    };

    printf("PASSED\n");
    return EXIT_SUCCESS;
}
