#include <stdio.h>
#include <stdlib.h>

#include "stack_ll_t.c"

// testing macros
#define FAIL() printf("\nFAILURE in %s %s:%d \n", __FILE__, __func__, __LINE__)
#define _assert(test) do { if (!(test)) { FAIL(); return 1; } } while(0)
#define _verify(test) do { int r=test(); tests_run++; if(r) return r; } while(0)

int static tests_run = 0;

int
test_create_stack_ll()
{
    int data = 10;
    int element_size = sizeof(data);

    stack_ll_t* head = new_stack_ll((void*)&data, element_size);

    int actual;
    memcpy(&actual, head->data, element_size);
    _assert(actual == data);

    _assert(head->element_size == element_size);

    _assert(head->next == NULL);

    destroy_stack_ll(head);

    return EXIT_SUCCESS;
};

int
test_stack_ll_push()
{
    int data = 10;
    int element_size = sizeof(data);

    stack_ll_t* null_test = NULL;

    int err = push_stack_ll(&null_test, &data, element_size);

    _assert(err == 1);

    stack_ll_t* head = new_stack_ll((void*)&data, element_size);

    int test = 5;

   err = push_stack_ll(&head, (void*)&test, element_size);

    _assert(err == 0);

    int actual;
    memcpy(&actual, head->data, element_size);
    _assert(actual == test);

    _assert(head->element_size == element_size);

    _assert(head->next != NULL);

    stack_ll_t* next = head->next;

    memcpy(&actual, next->data, element_size);
    _assert(actual == data);

    _assert(next->element_size == element_size);

    _assert(next->next == NULL);

    destroy_stack_ll(null_test);
    destroy_stack_ll(head);

   return EXIT_SUCCESS;
}

int
test_stack_ll_pop()
{
    int data = 10;
    int element_size = sizeof(data);

    int actual;
    stack_ll_t* null_test = NULL;

    int err = pop_stack_ll(&null_test, &actual);

    _assert(err == 1);

    stack_ll_t* head = new_stack_ll((void*)&data, element_size);

    err = pop_stack_ll(&head, &actual);

    _assert(err == 0);

    _assert(actual == data);

    _assert(head == NULL);

    destroy_stack_ll(null_test);
    destroy_stack_ll(head);

    return EXIT_SUCCESS;
};

int
test_stack_ll_is_empty()
{
    int data = 10;
    int element_size = sizeof(data);

    stack_ll_t* null_test = NULL;

    _assert(stack_ll_is_empty(null_test) == 1);

    stack_ll_t* head = new_stack_ll((void*)&data, element_size);

    _assert(stack_ll_is_empty(head) == 0);

    destroy_stack_ll(null_test);
    destroy_stack_ll(head);

    return EXIT_SUCCESS;
}

int
all_stack_ll_tests()
{
    _verify(test_create_stack_ll);
    _verify(test_stack_ll_push);
    _verify(test_stack_ll_pop);
    _verify(test_stack_ll_is_empty);

    printf("SUCCESS in %s: ran %d tests\n", __FILE__, tests_run);

    return EXIT_SUCCESS;
}
