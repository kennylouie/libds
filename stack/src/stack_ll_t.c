#include <stdlib.h>
#include <stdio.h>

#include "stack_ll_t.h"

stack_ll_t*
new_stack_ll(void* data, int element_size)
{
    stack_ll_t* new = malloc(sizeof(stack_ll_t));

    new->element_size = element_size;

    new->data = malloc(element_size);

    memcpy(new->data, data, element_size);

    new->next = NULL;

    return new;
};

int
push_stack_ll(stack_ll_t** s, void* data, int element_size)
{

    if (*s == NULL)
        return EXIT_FAILURE;

    stack_ll_t* new = malloc(sizeof(stack_ll_t));

    new->element_size = element_size;

    new->data = malloc(element_size);

    memcpy(new->data, data, element_size);

    new->next = *s;

    *s = new;

    return EXIT_SUCCESS;
};

int
pop_stack_ll(stack_ll_t** s, void* target)
{
    if (*s == NULL)
        return EXIT_FAILURE;

    stack_ll_t* head = *s;

    memcpy(target, head->data, head->element_size);

    *s = head->next;

    free(head->data);
    free(head);

    return EXIT_SUCCESS;
};

int
stack_ll_is_empty(stack_ll_t* s)
{
    return !s;
}

int
peek_stack_ll(stack_ll_t* s, void* target)
{
    if (s == NULL)
        return EXIT_FAILURE;

    memcpy(target, s->data, s->element_size);

    return EXIT_SUCCESS;
}

int
destroy_stack_ll(stack_ll_t* s)
{
    stack_ll_t* current = s;
    stack_ll_t* next;

    while (current != NULL)
    {
        next = current->next;
        free(current->data);
        free(current);
        current = next;
    };

    return EXIT_SUCCESS;
};
