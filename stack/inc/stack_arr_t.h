#ifndef STACK_ARR_H
#define STACK_ARR_H

typedef struct {
    int top;
    int capacity;
    void* data;
    unsigned element_size;
} stack_arr_t;

stack_arr_t* new_stack_arr(int capacity, unsigned element_size);

int stack_arr_is_empty(stack_arr_t* s);

int extend_stack_arr(stack_arr_t* s);

int stack_arr_is_full(stack_arr_t* s);

int push_stack_arr(stack_arr_t* s, void* element);

int pop_stack_arr(stack_arr_t* s, void* element);

int peek_stack_arr(stack_arr_t* s, void* element);

int destroy_stack_arr(stack_arr_t* s);

#endif
