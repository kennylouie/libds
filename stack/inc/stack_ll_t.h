#ifndef STACK_LL_H
#define STACK_LL_H

typedef struct Node {
    void* data;
    int element_size;
    struct Node* next;
} stack_ll_t;

stack_ll_t* new_stack_ll(void* data, int element_size);

int push_stack_ll(stack_ll_t** s, void* data, int element_size);

int pop_stack_ll(stack_ll_t** s, void* target);

int stack_ll_is_empty(stack_ll_t* s);

int peek_stack_ll(stack_ll_t* s, void* target);

int destroy_stack_ll(stack_ll_t* s);

#endif
