#include <stdio.h>
#include <stdlib.h>

#include "queue_ll_t.h"

node_ll_t*
new_node_ll(void* data, int element_size)
{
    node_ll_t* new = malloc(sizeof(node_ll_t));

    new->element_size = element_size;

    new->next = NULL;

    new->data = malloc(element_size);

    memcpy(new->data, data, element_size);

    return new;
};

queue_ll_t*
new_queue_ll()
{
    queue_ll_t* new = malloc(sizeof(queue_ll_t));

    new->front = new->rear = NULL;

    return new;
};

int
queue_ll_is_empty(queue_ll_t* q)
{
    return !q->rear;
};

int
enqueue_queue_ll(queue_ll_t* q, void* data, int element_size)
{
    node_ll_t* n = new_node_ll(data, element_size);

    if (queue_ll_is_empty(q))
    {
        q->front = q->rear = n;
        return EXIT_SUCCESS;
    }

    q->rear->next = n;
    q->rear = n;

    return EXIT_SUCCESS;
};

int
dequeue_queue_ll(queue_ll_t* q, void* target)
{
    if (queue_ll_is_empty(q))
        return EXIT_FAILURE;

    node_ll_t* temp = q->front;

    memcpy(target, q->front->data, q->front->element_size);

    q->front = q->front->next;

    if (q->front == NULL)
        q->rear = NULL;

    free(temp->data);
    free(temp);

    return EXIT_SUCCESS;
};
