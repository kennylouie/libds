#include <stdio.h>
#include <stdlib.h>

#include "queue_arr_t.h"

queue_arr_t*
new_queue_arr(unsigned capacity, int element_size)
{
    queue_arr_t* new = malloc(sizeof(queue_arr_t));

    new->size = new->front = 0;

    new->capacity = capacity;

    new->rear = capacity - 1;

    new->element_size = element_size;

    new->data = malloc(capacity * element_size);

    return new;
};

int
queue_arr_is_full(queue_arr_t* q)
{
    return q->size == (int)q->capacity;
};

int
queue_arr_is_empty(queue_arr_t* q)
{
    return q->size == 0;
};

int
enqueue_queue_arr(queue_arr_t* q, void* data)
{
    if (queue_arr_is_full(q))
        return EXIT_FAILURE;

    q->size++;

    q->rear = (q->rear + 1)%q->capacity;

    void* target = (char*)q->data + (q->rear * q->element_size);

    memcpy(target, data, q->element_size);

    return EXIT_SUCCESS;
};

int
dequeue_queue_arr(queue_arr_t* q, void* target)
{
    if (queue_arr_is_empty(q))
        return EXIT_FAILURE;

    q->size--;

    memcpy(target, (char*)q->data + (q->front * q->element_size), q->element_size);

    q->front = (q->front + 1)%q->capacity;

    return EXIT_SUCCESS;
};

int
front_queue_arr(queue_arr_t* q, void* target)
{
    if (queue_arr_is_empty(q))
        return EXIT_FAILURE;

    memcpy(target, (char*)q->data + (q->front * q->element_size), q->element_size);

    return EXIT_SUCCESS;
};

int
rear_queue_arr(queue_arr_t* q, void* target)
{
    if (queue_arr_is_empty(q))
        return EXIT_FAILURE;

    memcpy(target, (char*)q->data + (q->rear * q->element_size), q->element_size);

    return EXIT_SUCCESS;
};

int
destroy_queue_arr(queue_arr_t* q)
{
    free(q->data);
    free(q);

    return EXIT_SUCCESS;
};
