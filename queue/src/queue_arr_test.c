#include <stdio.h>
#include <stdlib.h>

#include "queue_arr_t.c"

// testing macros
#define FAIL() printf("\nFAILURE in %s %s:%d \n", __FILE__, __func__, __LINE__)
#define _assert(test) do { if (!(test)) { FAIL(); return 1; } } while(0)
#define _verify(test) do { int r=test(); tests_run++; if(r) return r; } while(0)

int static tests_run = 0;

int
test_create_queue_arr()
{
    unsigned capacity = 2;
    int element_size = sizeof(int);

    queue_arr_t* q = new_queue_arr(capacity, element_size);

    _assert(q->capacity == capacity);
    _assert(q->element_size == element_size);
    _assert(q->size == 0);
    _assert(q->front == 0);
    _assert(q->rear == (int)capacity - 1);
    _assert(sizeof((char*)q->data) == capacity * element_size);

    destroy_queue_arr(q);

    return EXIT_SUCCESS;
};

int
test_enqueue_queue_arr()
{
    unsigned capacity = 2;
    int element_size = sizeof(char) * 5;

    queue_arr_t* q = new_queue_arr(capacity, element_size);

    char* expected = "test";

    int err = enqueue_queue_arr(q, expected);

    char* actual = malloc(element_size);
    memcpy(actual, (char*)q->data + (q->rear * q->element_size), element_size);

    _assert(err == 0);
    _assert(q->size == 1);
    _assert(q->front == 0);
    _assert(q->rear == 0);
    _assert(q->element_size == element_size);
    _assert(strcmp(actual, expected) == 0);

    free(actual);
    destroy_queue_arr(q);

    return EXIT_SUCCESS;
}

int
test_enqueue_queue_arr_full()
{
    unsigned capacity = 2;
    int element_size = sizeof(char) * 5;

    queue_arr_t* q = new_queue_arr(capacity, element_size);

    char* expected = "test";

    int err = enqueue_queue_arr(q, expected);
    _assert(err == 0);

    err = enqueue_queue_arr(q, expected);
    _assert(err == 0);

    err = enqueue_queue_arr(q, expected);
    _assert(err == 1);

    _assert(q->size == (int)capacity);
    _assert(q->front == 0);
    _assert(q->rear == (int)capacity - 1);
    _assert(q->element_size == element_size);

    destroy_queue_arr(q);

    return EXIT_SUCCESS;
};

int
test_queue_arr_is_full()
{
    unsigned capacity = 1;
    int element_size = sizeof(char) * 5;

    queue_arr_t* q = new_queue_arr(capacity, element_size);

    _assert(queue_arr_is_full(q) == 0);

    char* expected = "test";

    enqueue_queue_arr(q, expected);

    _assert(queue_arr_is_full(q) == 1);

    destroy_queue_arr(q);

    return EXIT_SUCCESS;
};

int
test_queue_arr_is_empty()
{
    unsigned capacity = 1;
    int element_size = sizeof(char) * 5;

    queue_arr_t* q = new_queue_arr(capacity, element_size);

    _assert(queue_arr_is_empty(q) == 1);

    char* expected = "test";

    enqueue_queue_arr(q, expected);

    _assert(queue_arr_is_empty(q) == 0);

    destroy_queue_arr(q);

    return EXIT_SUCCESS;
};

int
test_dequeue_queue_arr()
{
    unsigned capacity = 2;
    int element_size = sizeof(char) * 5;

    queue_arr_t* q = new_queue_arr(capacity, element_size);

    char* expected = "test";

    enqueue_queue_arr(q, expected);

    char* actual = malloc(element_size);

    int err = dequeue_queue_arr(q, actual);

    _assert(err == 0);
    _assert(q->size == 0);
    _assert(q->front == 1);
    _assert(q->rear == 0);
    _assert(q->element_size == element_size);
    _assert(strcmp(actual, expected) == 0);

    free(actual);
    destroy_queue_arr(q);

    return EXIT_SUCCESS;
};

int
test_dequeue_queue_arr_empty()
{
    unsigned capacity = 2;
    int element_size = sizeof(char) * 5;

    queue_arr_t* q = new_queue_arr(capacity, element_size);

    char* actual = malloc(element_size);

    int err = dequeue_queue_arr(q, actual);

    _assert(err == 1);

    free(actual);
    destroy_queue_arr(q);

    return EXIT_SUCCESS;
};

int
test_front_rear_queue_arr()
{
    unsigned capacity = 2;
    int element_size = sizeof(char) * 5;

    queue_arr_t* q = new_queue_arr(capacity, element_size);

    char* expected_front = "test";

    char* actual_front = malloc(element_size);
    int err = front_queue_arr(q, actual_front); _assert(err == 1);

    enqueue_queue_arr(q, expected_front);

    err = front_queue_arr(q, actual_front);

    _assert(err == 0);
    _assert(q->size == 1);
    _assert(q->front == 0);
    _assert(q->rear == 0);
    _assert(q->element_size == element_size);
    _assert(strcmp(actual_front, expected_front) == 0);

    char* expected_rear = "rear";

    enqueue_queue_arr(q, expected_rear);

    char* actual_rear = malloc(element_size);
    err = rear_queue_arr(q, actual_rear);

    _assert(err == 0);
    _assert(q->size == 2);
    _assert(q->front == 0);
    _assert(q->rear == 1);
    _assert(q->element_size == element_size);
    _assert(strcmp(actual_rear, expected_rear) == 0);

    free(actual_front);
    free(actual_rear);
    destroy_queue_arr(q);

    return EXIT_SUCCESS;
};

int
all_queue_arr_tests()
{
    _verify(test_create_queue_arr);
    _verify(test_enqueue_queue_arr);
    _verify(test_enqueue_queue_arr_full);
    _verify(test_queue_arr_is_full);
    _verify(test_queue_arr_is_empty);
    _verify(test_dequeue_queue_arr);
    _verify(test_dequeue_queue_arr_empty);
    _verify(test_front_rear_queue_arr);

    printf("SUCCESS in %s: ran %d tests\n", __FILE__, tests_run);

    return EXIT_SUCCESS;
};
