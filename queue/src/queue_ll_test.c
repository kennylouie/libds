#include <stdio.h>
#include <stdlib.h>

#include "queue_ll_t.c"

// testing macros
#define FAIL() printf("\nFAILURE in %s %s:%d \n", __FILE__, __func__, __LINE__)
#define _assert(test) do { if (!(test)) { FAIL(); return 1; } } while(0)
#define _verify(test) do { int r=test(); tests_run++; if(r) return r; } while(0)

int static tests_run = 0;

int
test_new_node_ll()
{
    int element_size = sizeof(int);

    int actual = 5;

    node_ll_t* n = new_node_ll(&actual, element_size);

    _assert(n->element_size == element_size);
    _assert(n->next == NULL);

    int expected;
    memcpy(&expected, n->data, element_size);

    _assert(actual == expected);

    free(n->data);
    free(n);

    return EXIT_SUCCESS;
};

int
test_new_queue_ll()
{
    queue_ll_t* q = new_queue_ll();

    _assert(q->front == NULL);
    _assert(q->rear == NULL);

    return EXIT_SUCCESS;
};

int
test_queue_ll_is_empty()
{
    queue_ll_t* q = new_queue_ll();

    _assert(queue_ll_is_empty(q) == 1);

    return EXIT_SUCCESS;
};

int
test_enqueue_queue_ll()
{
    queue_ll_t* q = new_queue_ll();

    char* expected = "test";
    int element_size = strlen(expected) + 1;

    int err = enqueue_queue_ll(q, expected, element_size);

    _assert(err == 0);
    _assert(q->front == q->rear);

    char* actual = malloc(element_size);

    memcpy(actual, q->rear->data, q->rear->element_size);

    _assert(strcmp(actual, expected) == 0);

    int next = 5;
    element_size = sizeof(next);

    err = enqueue_queue_ll(q, &next, element_size);

    _assert(err == 0);
    _assert(q->front != q->rear);

    int next_actual;

    memcpy(actual, q->front->data, q->front->element_size);
    _assert(strcmp(actual, expected) == 0);

    memcpy(&next_actual, q->front->next->data, q->front->next->element_size);
    _assert(next_actual == next);
    _assert(q->rear->next == NULL);

    return EXIT_SUCCESS;
};

int
test_dequeue_queue_ll()
{
    queue_ll_t* q = new_queue_ll();

    int expected = 5;
    int element_size = sizeof(expected);

    enqueue_queue_ll(q, &expected, element_size);

    int next = 10;
    enqueue_queue_ll(q, &next, element_size);

    _assert(q->front != q->rear);

    int actual;

    int err = dequeue_queue_ll(q, &actual);

    _assert(err == 0);
    _assert(expected == actual);
    _assert(q->front == q->rear);
    _assert(q->front != NULL);

    int next_actual;
    err = dequeue_queue_ll(q, &next_actual);

    _assert(err == 0);
    _assert(next == next_actual);
    _assert(q->front ==  q->rear);
    _assert(q->front == NULL);

    err = dequeue_queue_ll(q, &actual);
    _assert(err == 1);

    return EXIT_SUCCESS;
};

int
all_queue_ll_tests()
{
    _verify(test_new_node_ll);
    _verify(test_new_queue_ll);
    _verify(test_queue_ll_is_empty);
    _verify(test_enqueue_queue_ll);
    _verify(test_dequeue_queue_ll);

    printf("SUCCESS in %s: ran %d tests\n", __FILE__, tests_run);

    return EXIT_SUCCESS;
};
