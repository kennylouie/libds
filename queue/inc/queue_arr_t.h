#ifndef QUEUE_ARR_H
#define QUEUE_ARR_H

typedef struct Queue {
    int size, front, rear;
    unsigned capacity;
    void* data;
    int element_size;
} queue_arr_t;

queue_arr_t* new_queue_arr(unsigned capacity, int element_size);

int queue_arr_is_full(queue_arr_t* q);

int queue_arr_is_empty(queue_arr_t* q);

int enqueue_queue_arr(queue_arr_t* q, void* data);

int dequeue_queue_arr(queue_arr_t* q, void* target);

int front_queue_arr(queue_arr_t* q, void* target);

int rear_queue_arr(queue_arr_t* q, void* target);

int destroy_queue_arr(queue_arr_t* q);

#endif
