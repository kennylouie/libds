#ifndef QUEUE_LL_H
#define QUEUE_LL_H

typedef struct Node {
    void* data;
    int element_size;
    struct Node* next;
} node_ll_t;

typedef struct Queue {
    node_ll_t* front;
    node_ll_t* rear;
} queue_ll_t;

node_ll_t* new_node_ll(void* data, int element_size);

queue_ll_t* new_queue_ll();

int enqueue_queue_ll(queue_ll_t* q, void* data, int element_size);

int dequeue_queue_ll(queue_ll_t* q, void* target);

int queue_ll_is_empty(queue_ll_t* q);

#endif
