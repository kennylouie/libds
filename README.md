# libds

## Background

A C utility library to expose and API for implementing different data structures and associated algorithms.

Originally intended to be a learning exercise. It is now structured as header files to be easily exportable.

This project is a WIP and will continue to include algorithms.

## Development

### Tests

Makefile in each datastructure subdirectory.

```
make test
make clean
make cleaner
```
