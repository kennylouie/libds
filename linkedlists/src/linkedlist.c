#include <stdio.h>
#include <stdlib.h>
#include "../inc/linkedlist.h"

// O(1) insert node at beginning of linkedlist as new head
int
linked_list_push(linked_list_t** head, int new_data)
{
	if (*head == NULL) return EXIT_FAILURE;

        linked_list_t* new_node = (linked_list_t*)malloc(sizeof(linked_list_t));
	new_node->data = new_data;
	new_node->next = *head;
	*head = new_node;

	return EXIT_SUCCESS;
}

// O(1) insert node after a given previous node
int
linked_list_insert_after(linked_list_t* prev, int new_data)
{
	if (prev == NULL) return EXIT_FAILURE;

        linked_list_t* new_node = (linked_list_t*)malloc(sizeof(linked_list_t));
	new_node->data = new_data;
	new_node->next = prev->next;
	prev->next = new_node;

	return EXIT_SUCCESS;
}

// O(n) insert node at end of linkedlist
// Need to traverse through entire linked list
int
linked_list_append(linked_list_t** head, int new_data)
{
        linked_list_t* new_node = (linked_list_t*)malloc(sizeof(linked_list_t));
	new_node->data = new_data;
	new_node->next = NULL;

	if (*head == NULL)
	{
                *head = new_node;
		return EXIT_SUCCESS;
	}

	linked_list_t* last = *head;
	while (last->next != NULL)
		last = last->next;

	last->next = new_node;
	return EXIT_SUCCESS;
}

void
print_linked_list(linked_list_t* l)
{
	while (l != NULL) {
		printf("%d\n", l->data);
		l = l->next;
	}
}
