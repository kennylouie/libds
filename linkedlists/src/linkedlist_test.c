#include <stdio.h>
#include <stdlib.h>
#include "../inc/linkedlist.h"

// testing macros
#define FAIL() printf("\nfailure in %s() line %d\n", __func__, __LINE__)
#define _assert(test) do { if (!(test)) { FAIL(); return 1; } } while(0)
#define _verify(test) do { int r=test(); tests_run++; if(r) return r; } while(0)

int static tests_run = 0;

// test insert new node at beginning
int
insert_new_node_at_beginning()
{
        linked_list_t* head = (linked_list_t*)malloc(sizeof(linked_list_t));
        head->data = 0;
	head->next = NULL;

	int err = linked_list_push(&head, 1);
	if (err) FAIL();

	int expected[] = {1, 0};

	for (int i = 0; i < (sizeof(expected)/sizeof(int)); i++)
	{
        	_assert(head->data == expected[i]);
		head = head->next;
	}

	// free memory of linked list nodes
	linked_list_t* tmp;

	while (head != NULL)
	{
		tmp = head;
		head = head->next;
		free(tmp);
	}

	return EXIT_SUCCESS;
}

// expect fail for push if node is NULL
int
fail_push_head_null()
{
        linked_list_t* head = (linked_list_t*)malloc(sizeof(linked_list_t));
	head = NULL;
	int err = linked_list_push(&head, 1);
	_assert(err == 1);

	return EXIT_SUCCESS;
}

// test insert new node after given node
int
insert_new_node_after_given_node()
{
        linked_list_t* head = (linked_list_t*)malloc(sizeof(linked_list_t));
        head->data = 0;
	head->next = NULL;

	int err = linked_list_insert_after(head, 1);
	if (err) FAIL();

	int expected[] = {0, 1};

	for (int i = 0; i < (sizeof(expected)/sizeof(int)); i++)
	{
        	_assert(head->data == expected[i]);
		head = head->next;
	}

	// free memory of linked list nodes
	linked_list_t* tmp;

	while (head != NULL)
	{
		tmp = head;
		head = head->next;
		free(tmp);
	}

	return EXIT_SUCCESS;
}

// expect fail for insert after if prev node is NULL
int
fail_insert_prev_null()
{
        linked_list_t* head = (linked_list_t*)malloc(sizeof(linked_list_t));
	head = NULL;
	int err = linked_list_insert_after(head, 1);
	_assert(err == 1);

	return EXIT_SUCCESS;
}

// test append new node at end
int
append_new_node_at_end()
{
        linked_list_t* head = (linked_list_t*)malloc(sizeof(linked_list_t));
        head->data = 0;
	head->next = NULL;

	int err = linked_list_append(&head, 1);
	if (err) FAIL();

	int expected[] = {0, 1};

	for (int i = 0; i < (sizeof(expected)/sizeof(int)); i++)
	{
        	_assert(head->data == expected[i]);
		head = head->next;
	}

	// free memory of linked list nodes
	linked_list_t* tmp;

	while (head != NULL)
	{
		tmp = head;
		head = head->next;
		free(tmp);
	}

	return EXIT_SUCCESS;
}

// test append new node at beginning when head is NULL
int
append_new_node_at_beginning()
{
        linked_list_t* head = (linked_list_t*)malloc(sizeof(linked_list_t));
	head = NULL;

	int err = linked_list_append(&head, 1);
	if (err) FAIL();

	int expected = 1;

        _assert(head->data == expected);

	// free memory of linked list nodes
	linked_list_t* tmp;

	while (head != NULL)
	{
		tmp = head;
		head = head->next;
		free(tmp);
	}

	return EXIT_SUCCESS;
}
int
all_tests()
{
	_verify(insert_new_node_at_beginning);
	_verify(insert_new_node_after_given_node);
	_verify(fail_push_head_null);
	_verify(fail_insert_prev_null);
	_verify(append_new_node_at_end);
	_verify(append_new_node_at_beginning);
	return EXIT_SUCCESS;
}

int
main()
{
	int result = all_tests();
	if (result == 0) {
        	printf("PASSED\n");
	}

	printf("Tests run: %d\n", tests_run);

	return result != EXIT_SUCCESS;
}
