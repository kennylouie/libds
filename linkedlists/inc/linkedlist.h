#ifndef LINKEDLIST_H
#define LINKEDLIST_H
/*
 * Implementation of linked list in C
 * Description: linear data structure not in contiguous memory where the elements are linked together by pointers
 *
 *  +-------------------------------+           +-------------------------------+
 *  |    A: value, pointer_to_B     |           |    B: value, pointer_to_C     |
 *  |                               |---------->|                               |
 *  +-------------------------------+           +-------------------------------+
 *
 * Time complexity:
 * 	Access is O(n)
 * 	Search is O(n)
 * 	Insert is O(1)
 * 	Delete is O(1)
 * Space complexity: O(n)
 *
 * linked lists have the advantage of not being
 * bound by the size of allocation and are not
 * contiguous in memory location, i.e. like in * arrays * thus, singly linked lists are dynamic * in nature and can grow and shrink as needed
 * insertion and deletion are cheap for singly linked lists because of the pointer nature as
 * opposed to arrays where arrays possibly have to be moved and resized
 * drawbacks include having to store extra memory for the pointers
 * random access is not allowed due to the pointer nature of the data structure
 * data needs to be accessed from the beginning (head node)
 * access is O(n) because searching starts from the beginning of the data structure and follows the pathing until the index desired
 */

typedef struct Node {
	/*
	 * first node is the head
	 * if linked list is empty, the next pointer points to NULL
	 */
	int data;
	struct Node* next;
} linked_list_t;

void print_linked_list(linked_list_t* l);

int linked_list_push(linked_list_t** head, int new_data);

int linked_list_insert_after(linked_list_t* prev, int new_data);

int linked_list_append(linked_list_t** head, int new_data);

#endif
